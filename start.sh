#!/bin/bash
# run this script before working on the code.
# source start.sh

if [[ ! -d .ve ]]; then
  echo "create virtual environment for python3."
  mkdir .ve
  virtualenv .ve -p python3
fi

echo "activating virtual environment"
source .ve/bin/activate

pip3 install -r toolkit/pip/requirements.txt
