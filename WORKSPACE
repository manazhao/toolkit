workspace(name = "com_github_manazhao_toolkit")

load("@bazel_tools//tools/build_defs/repo:git.bzl", "git_repository")
load("@bazel_tools//tools/build_defs/repo:git.bzl", "new_git_repository")
load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")

git_repository(
    name = "com_google_absl",
    commit = "9e94e48",
    remote = "https://github.com/abseil/abseil-cpp.git",
)

bind(
    name = "absl_strings",
    actual = "@com_google_absl//absl/strings",
)

bind(
    name = "absl_optional",
    actual = "@com_google_absl//absl/types:optional",
)

bind(
    name = "absl_time",
    actual = "@com_google_absl//absl/time",
)

git_repository(
    name = "com_google_absl_py",
    commit = "5218231",
    remote = "https://github.com/abseil/abseil-py.git",
)

bind(
    name = "absl_flags_py",
    actual = "@com_google_absl_py//absl/flags",
)

bind(
    name = "absl_logging_py",
    actual = "@com_google_absl_py//absl/logging",
)

bind(
    name = "absl_app_py",
    actual = "@com_google_absl_py//absl:app",
)

git_repository(
    name = "com_google_exegesis",
    remote = "https://github.com/google/EXEgesis",
    tag = "v3.4.1",
)

git_repository(
    name = "com_googlesource_code_re2",
    remote = "https://github.com/google/re2.git",
    tag = "2017-12-01",
)

bind(
    name = "google_re2",
    actual = "@com_googlesource_code_re2//:re2",
)

git_repository(
    name = "com_github_google_googletest",
    commit = "2fe3bd994b3189899d93f1d5a881e725e046fdc2",
    remote = "https://github.com/google/googletest",
    shallow_since = "1535728917 -0400",
)

bind(
    name = "gtest",
    actual = "@com_github_google_googletest//:gtest",
)

bind(
    name = "gtest_main",
    actual = "@com_github_google_googletest//:gtest_main",
)

git_repository(
    name = "com_github_gflags_gflags",
    remote = "https://github.com/gflags/gflags.git",
    tag = "v2.2.2",
)

bind(
    name = "gflags",
    actual = "@com_github_gflags_gflags//:gflags",
)

git_repository(
    name = "com_github_google_glog",
    remote = "https://github.com/google/glog.git",
    tag = "v0.4.0",
)

bind(
    name = "glog",
    actual = "@com_github_google_glog//:glog",
)

# protobuf related settings.
# Six package is required by @com_google_protobuf project.
http_archive(
    name = "rules_python",
    sha256 = "aa96a691d3a8177f3215b14b0edc9641787abaaa30363a080165d06ab65e1161",
    url = "https://github.com/bazelbuild/rules_python/releases/download/0.0.1/rules_python-0.0.1.tar.gz",
)

http_archive(
    name = "six_archive",
    build_file = "@//:six.BUILD",
    sha256 = "105f8d68616f8248e24bf0e9372ef04d3cc10104f1980f54d57b2ce73a5ad56a",
    urls = ["https://pypi.python.org/packages/source/s/six/six-1.10.0.tar.gz#md5=34eed507548117b2ab523ab14b2f8b55"],
)

bind(
    name = "six",
    actual = "@six_archive//:six",
)

http_archive(
    name = "com_google_protobuf",
    sha256 = "73fdad358857e120fd0fa19e071a96e15c0f23bb25f85d3f7009abfd4f264a2a",
    strip_prefix = "protobuf-3.6.1.3",
    url = "https://github.com/google/protobuf/archive/v3.6.1.3.tar.gz",
)

bind(
    name = "protobuf",
    actual = "@com_google_protobuf//:protobuf",
)

bind(
    name = "protobuf_python",
    actual = "@com_google_protobuf//:protobuf_python",
)
# End of setup for python protobuf.
