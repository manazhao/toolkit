#ifndef TOOLKIT_COMMON_UTIL_H_
#define TOOLKIT_COMMON_UTIL_H_

#include <fstream>
#include <functional>
#include <string>
#include <vector>

#include "absl/strings/str_cat.h"
#include "absl/strings/str_replace.h"
#include "glog/logging.h"
#include "google/protobuf/any.pb.h"
#include "google/protobuf/message.h"
#include "google/protobuf/text_format.h"

namespace toolkit {
<<<<<<< HEAD:toolkit/common/utils.h
=======
namespace ml {
>>>>>>> 26d554e48bb66e5918d93d6749ae5118daf7268d:common/utils.h

bool ReadCsvFile(
    const std::string &filename, const std::string &separator,
    const std::function<bool(const std::vector<std::string> &)> &on_fields_cb);

bool WriteProtoText(const std::string &filename,
                    const ::google::protobuf::Message &message);

bool ReadProtoText(const std::string &filename,
                   ::google::protobuf::Message *message);

template <typename T>
bool AnyToType(const ::google::protobuf::Any &any, T *message) {
  if (!any.Is<T>()) {
    return false;
  }
  any.UnpackTo(message);
  return true;
}

template <typename T> T AnyToTypeOrDie(const ::google::protobuf::Any &any) {
  T message;
  CHECK(AnyToType(any, &message)) << "Error in unpacking from any: "
                                  << any.DebugString();
  return message;
}

template <typename T>::google::protobuf::Any TypeToAny(const T &message) {
  ::google::protobuf::Any any;
  any.PackFrom(message);
  return any;
}

// Replace variables defined in the form of "$variable_name" with specific
// values. Each relacement is a key value pair where key is the variable name
// and value is the specific value.
// Example: replace ${trial_id} with "abc".
// Message message;
// ReplaceVariableForMessage({{"${trial_id}","abc"}}, &message);
template <typename T>
void ReplaceVariableForMessage(
    const std::vector<std::pair<std::string, std::string>> &replacement,
    T *message) {
  T updated_message;
  CHECK(::google::protobuf::TextFormat::ParseFromString(
      absl::StrReplaceAll(message->DebugString(), replacement),
      &updated_message))
      << "Error in parsing Message from text.";
  message->Swap(&updated_message);
}

// Generate a unique id for the message.
template <typename T> std::string GenerateMessageId(const T &message) {
  return absl::StrCat(absl::Hex(std::hash<std::string>()(message.DebugString()),
                                absl::kZeroPad16));
}

<<<<<<< HEAD:toolkit/common/utils.h
=======
} // namespace ml
>>>>>>> 26d554e48bb66e5918d93d6749ae5118daf7268d:common/utils.h
} // namespace toolkit

#endif
