#include "toolkit/common/utils.h"

#include <fstream>
#include <iostream>
#include <vector>

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "absl/strings/str_split.h"
#include "glog/logging.h"
#include "google/protobuf/io/zero_copy_stream_impl.h"
#include "google/protobuf/text_format.h"

namespace toolkit {
<<<<<<< HEAD:toolkit/common/utils.cc
=======
namespace ml {
>>>>>>> 26d554e48bb66e5918d93d6749ae5118daf7268d:common/utils.cc

using ::google::protobuf::Message;
using ::google::protobuf::TextFormat;
using ::google::protobuf::io::FileOutputStream;
using ::google::protobuf::io::FileInputStream;

bool ReadCsvFile(
    const std::string &filename, const std::string &separator,
    const std::function<bool(const std::vector<std::string> &)> &on_fields_cb) {
  std::ifstream ifs(filename);
  if (!ifs.good()) {
    LOG(ERROR) << "failed to open file for read: " << filename;
    return false;
  }
  std::string line;
  while (std::getline(ifs, line)) {
    const std::vector<std::string> fields = absl::StrSplit(line, separator);
    if (!on_fields_cb(fields)) {
      LOG(ERROR) << "Error in consuming fields from line: " << line;
      return false;
    }
  }
  return true;
}

bool WriteProtoText(const std::string &filename, const Message &message) {
  int file_id = open(filename.c_str(), O_WRONLY | O_CREAT, S_IRWXU);
  if (file_id < 0) {
    LOG(ERROR) << "Error in opening file for writing: " << filename;
    return false;
  }
  FileOutputStream fos(file_id);
  return TextFormat::Print(message, &fos);
}

bool ReadProtoText(const std::string &filename, Message *message) {
  int file_id = open(filename.c_str(), O_RDONLY);
  if (file_id < 0) {
    LOG(ERROR) << "Error in opening file for reading: " << filename;
    return false;
  }
  FileInputStream fis(file_id);
  return TextFormat::Parse(&fis, message);
}

<<<<<<< HEAD:toolkit/common/utils.cc
=======
} // namespace ml
>>>>>>> 26d554e48bb66e5918d93d6749ae5118daf7268d:common/utils.cc
} // namespace toolkit
