#include "toolkit/common/utils.h"

#include "gmock/gmock.h"
#include "google/protobuf/text_format.h"
#include "google/protobuf/wrappers.pb.h"
#include "toolkit/common/googletest.h"
#include "toolkit/common/protobuf_matcher.h"
#include "gtest/gtest.h"

namespace toolkit {
<<<<<<< HEAD:toolkit/common/utils_test.cc
=======
namespace ml {
>>>>>>> 26d554e48bb66e5918d93d6749ae5118daf7268d:common/utils_test.cc

using ::google::protobuf::FloatValue;
using ::google::protobuf::StringValue;
using ::google::protobuf::TextFormat;
using ::testing::ElementsAreArray;
using ::testing::EqualsProto;

TEST(UtilsTest, TestCsvReader) {
  const std::string test_file =
      FLAGS_test_srcdir + "/toolkit/common/testdata/sample.csv";
  const std::vector<std::vector<std::string>> lines = {
      {"company", "ticker", "price"}, {"app1e", "app1", "100"}};
  int counter = 0;
  ASSERT_TRUE(
      ReadCsvFile(test_file, ",",
                  [&lines, &counter](const std::vector<std::string> &fields) {
                    EXPECT_THAT(fields, ElementsAreArray(lines[counter++]));
                    return true;
                  }));
}

TEST(UtilsTest, WriteProtoTextToFile) {
  FloatValue value;
  value.set_value(1.0);
  const std::string pbtxt_file = FLAGS_test_tmpdir + "/message.pbtxt";
  ASSERT_TRUE(WriteProtoText(pbtxt_file, value));
  // Read it back from the file and compare to its original value.
  FloatValue read_value;
  ASSERT_TRUE(ReadProtoText(pbtxt_file, &read_value));
  EXPECT_THAT(read_value, EqualsProto(value));
}

TEST(UtilsTest, AnyToTypeConversion) {
  FloatValue v;
  v.set_value(1.0);
  EXPECT_THAT(AnyToTypeOrDie<FloatValue>(TypeToAny(v)), EqualsProto(v));
}

TEST(UtilsTest, ReplaceMessageVariable) {
  StringValue message;
  message.set_value("${experiment_id} is $stock_name.");
  StringValue expected_message;
  TextFormat::ParseFromString(R"(
    value: "123 is abc.")",
                              &expected_message);
  ReplaceVariableForMessage(
      {{"${experiment_id}", "123"}, {"$stock_name", "abc"}}, &message);
  EXPECT_THAT(message, EqualsProto(expected_message));
}

TEST(UtilsTest, GenerateMessageId) {
  StringValue message;
  message.set_value("test message.");
  ASSERT_TRUE(!GenerateMessageId(message).empty());
}

<<<<<<< HEAD:toolkit/common/utils_test.cc
=======
} // namespace ml
>>>>>>> 26d554e48bb66e5918d93d6749ae5118daf7268d:common/utils_test.cc
} // namespace toolkit
