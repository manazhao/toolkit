#include "toolkit/common/googletest.h"

#include <cstdlib>
#include <string>

#include "gflags/gflags.h"

std::string GetTmpDir() {
  // 'bazel test' sets TEST_TMPDIR
  const char *env = getenv("TEST_TMPDIR");
  if (env && env[0] != '\0') {
    return env;
  }
  env = getenv("TMPDIR");
  if (env && env[0] != '\0') {
    return env;
  }
  return "/tmp";
}

std::string GetSrcDir() {
  // Bazel makes data dependencies available via a relative path.
  return ".";
}

DEFINE_string(test_tmpdir, GetTmpDir(), "test temp directory.");
DEFINE_string(test_srcdir, GetSrcDir(), "test source code directory.");
