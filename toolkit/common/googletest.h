#ifndef TOOLKIT_COMMON_GOOGLETEST_H_
#define TOOLKIT_COMMON_GOOGLETEST_H_

#include "gflags/gflags_declare.h"

DECLARE_string(test_tmpdir);
DECLARE_string(test_srcdir);

#define EXPECT_ISOKANDHOLDS(status_or_value, matchers)                         \
  EXPECT_OK((status_or_value));                                                \
  EXPECT_THAT((status_or_value).ValueOrDie(), (matchers))

#endif /* ifndef TOOLKIT_COMMON_GOOGLETEST_H_ */
