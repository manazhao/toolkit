syntax = "proto3";

package toolkit.ml;

import "toolkit/ml/distribution_strategy.proto";
import "toolkit/common/callable.proto";

message InputLayer {
  int32 batch_size = 1;
  repeated int32 shape = 2;
  string dtype = 3;
  bool sparse = 4;
}

message LambdaLayer {
  toolkit.common.CallableRegistry callable_registry = 1;
  repeated int32 output_shape = 2;
}

enum PaddingType {
  PADDING_TYPE_UNSPECIFIED = 0;
  PADDING_TYPE_SAME = 1;
  PADDING_TYPE_VALID = 2;
}

enum DataFormat {
  DATA_FORMAT_UNSPECIFIED = 0;
  DATA_FORMAT_CHANNELS_LAST = 1;
  DATA_FORMAT_CHANNELS_FIRST = 2;
}

enum ActivationType {
  ACTIVATION_TYPE_UNSPECIFIED = 0;
  ACTIVATION_TYPE_TAHN = 1;
  ACTIVATION_TYPE_SOFTMAX = 2;
  ACTIVATION_TYPE_ELU = 3;
  ACTIVATION_TYPE_SELU = 4;
  ACTIVATION_TYPE_SOFTPLUS = 5;
  ACTIVATION_TYPE_SOFTSIGN = 6;
  ACTIVATION_TYPE_RELU = 7;
  ACTIVATION_TYPE_SIGMOID = 8;
  ACTIVATION_TYPE_HARD_SIGMOID = 9;
  ACTIVATION_TYPE_EXPONENTIAL = 10;
  ACTIVATION_TYPE_LINEAR = 11;
}

message ActivationLayer { ActivationType type = 1; }

message Conv2DLayer {
  int32 filters = 1;
  repeated int32 kernel_size = 2;
  repeated int32 strides = 3;
  PaddingType padding = 4;
  DataFormat data_format = 5;
  bool use_bias = 6;
  ActivationType activation = 7;
}

message DenseLayer {
  int32 units = 1;
  ActivationType activation = 2;
  bool use_bias = 3;
  bool is_time_distributed = 4;
}

message DropoutLayer {
  float rate = 1;
  repeated int32 noise_shape = 2;
  int64 seed = 3;
}

message FlattenLayer {}

message LstmLayer {
  int32 units = 1;
  bool return_sequences = 2;
  bool use_bias = 3;
  bool go_backwards = 4;
}

message MaxPooling2DLayer {
  repeated int32 pool_size = 1;
  repeated int32 strides = 2;
  PaddingType padding = 3;
  DataFormat data_format = 4;
}

message ZeroPadding2DLayer {
  repeated int32 padding = 1;
  DataFormat data_format = 2;
}

message L2NormalizationLayer { float gamma_init = 1; }
message BatchNormalizationLayer {}

message CtcBatchCostLayer {
  string y_true = 1;
  string y_pred = 2;
  string input_length = 3;
  string label_length = 4;
}

message LayerConfig {
  string name = 1;
  repeated string dependency = 2;
  bool is_output = 3;
  oneof specific_layer {
    InputLayer input = 4;
    Conv2DLayer conv_2d = 5;
    MaxPooling2DLayer max_pooling_2d = 6;
    ActivationLayer activation = 7;
    ZeroPadding2DLayer zero_padding_2d = 8;
    L2NormalizationLayer l2_normalization = 9;
    DenseLayer dense = 10;
    FlattenLayer flatten = 11;
    DropoutLayer dropout = 12;
    CtcBatchCostLayer ctc_batch_cost = 13;
    LambdaLayer lambda_layer = 14;
    LstmLayer lstm_layer = 15;
    BatchNormalizationLayer batch_normalization_layer = 16;
  }
}

message SgdOptimizer {
  float lr = 1;
  float decay = 2;
  float momentum = 3;
  bool nesterov = 4;
}

message RMSpropOptimizer {
  float lr = 1;
  float rho = 2;
  float epsilon = 3;
  float decay = 4;
}

message AdagradOptimizer {
  float lr = 1;
  float epsilon = 2;
  float decay = 3;
}

message AdadeltaOptimizer {
  float lr = 1;
  float rho = 2;
  float epsilon = 3;
  float decay = 4;
}

message AdamOptmizer {
  float lr = 1;
  float beta_1 = 2;
  float beta_2 = 3;
  float epsilon = 4;
  bool amsgrad = 5;
}

enum LossType {
  LOSS_TYPE_UNSPECIFIED = 0;
  LOSS_TYPE_MEAN_SQUARED_ERROR = 1;
  LOSS_TYPE_MEAN_ABSOLUTE_ERROR = 2;
  LOSS_TYPE_MEAN_ABSOLUTE_PERCENTAGE_ERROR = 3;
  LOSS_TYPE_MEAN_SQUARED_LOGARITHMIC_ERROR = 4;
  LOSS_TYPE_SQUARED_HINGE = 5;
  LOSS_TYPE_HINGE = 6;
  LOSS_TYPE_CATEGORICAL_HINGE = 7;
  LOSS_TYPE_LOGCOSH = 8;
  LOSS_TYPE_CATEGORICAL_CROSSENTROPY = 9;
  LOSS_TYPE_SPARSE_CATEGORICAL_CROSSENTROPY = 10;
  LOSS_TYPE_BINARY_CROSSENTROPY = 11;
  LOSS_TYPE_KULLBACK_LEIBLER_DIVERGENCE = 12;
  LOSS_TYPE_POISSON = 13;
  LOSS_TYPE_COSIN_PROXIMITY = 14;
  LOSS_TYPE_CUSTOMIZED = 15;
}

enum MetricType {
  METRIC_TYPE_UNSPECIFIED = 0;
  METRIC_TYPE_MAE = 1;
  METRIC_TYPE_BINARY_ACCURACY = 2;
  METRIC_TYPE_CATEGORICAL_ACCURACY = 3;
  METRIC_TYPE_SPARSE_CATEGORICAL_ACCURACY = 4;
  METRIC_TYPE_TOP_K_CATEGORICAL_ACCURACY = 5;
  METRIC_TYPE_SPARSE_TOP_K_CATEGORICAL_ACCURACY = 6;
  METRIC_TYPE_MSE = 7;
  METRIC_TYPE_CUSTOMIZED = 8;
}

message LossSpec {
  string target_name = 1;
  oneof loss {
    LossType predefined = 2;
    // The loss function is registered in GlobalVariableRepository.
    string custom_function = 3;
  }
}

message MetricSpec {
  string target_name = 1;
  oneof metric {
    MetricType predefined = 2;
    // The metric function is registered in GlobalVariableRepository.
    string custom_function = 3;
  }
}

// A test function is usually used to obtain outputs from intermediate layers
// for debugging purpose.
message TestFunctionSpec {
  string name = 1;
  // Names of the input layers. The names must exist in model network.
  repeated string inputs = 2;

  // Names of the output layers. The names must exist in model network.
  repeated string outputs = 3;
}

message ModelConfig {
  string name = 1;
  string description = 2;

  // Model layers.
  repeated LayerConfig layer = 3;

  oneof specific_optimizer {
    SgdOptimizer sgd_optimizer = 4;
    RMSpropOptimizer rmsprop_optimizer = 5;
    AdagradOptimizer adagrad_optimizer = 6;
    AdadeltaOptimizer adadelta_optimizer = 7;
    AdamOptmizer adam_optimizer = 8;
  }
  repeated LossSpec loss_spec = 9;
  repeated MetricSpec metric_spec = 10;
  // Distribution strategy.
  toolkit.ml.DistributionStrategyConfig distribution_strategy = 12;

  repeated TestFunctionSpec test_functions = 13;
}
