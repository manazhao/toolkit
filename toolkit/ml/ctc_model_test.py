import unittest
import os

from absl import logging
from google.protobuf import text_format
from parameterized import parameterized
from typing import List

import numpy as np
import tensorflow as tf
from tensorflow.keras import backend as K

from toolkit.common import global_variable as gv
from toolkit.ml import train_pb2
from toolkit.common import global_variable as gv
from toolkit.ml.model_trainer import ModelTrainer
from toolkit.ml import input_pb2
from toolkit.ml import input as data_input

GVR = gv.GLOBAL_VARIABLE_REPOSITORY

tf.enable_eager_execution()


def _data_generator():
  while True:
    input_length = np.random.randint(low=5, high=10)
    # 10 features
    x = np.random.rand(input_length, 10, 1)
    # there are 10 classes.
    label_length = input_length - 2
    y = np.random.randint(low=0, high=9, size=label_length)
    yield {
        "features": x,
        "labels": y,
        "input_length": np.array([input_length]),
        "label_length": np.array([label_length])
    }


def _split_xy(feature_map):
  return feature_map, 0


GVR.register_callable("/callable/asr_data_generator", _data_generator)
GVR.register_callable("/callable/split_xy", _split_xy)

_DATASET_CONFIG_PBTXT = """
generator_dataset {
  callable_registry {
    function_name: "/callable/asr_data_generator"
  }
  output_types {
    key: "features"
    value: "float32"
  }
  output_types {
    key: "labels"
    value: "int32"
  }
  output_types {
    key: "input_length"
    value: "int32"
  }
  output_types {
    key: "label_length"
    value: "int32"
  }
  output_shapes {
    key: "features"
    value {
      values: [-1, 10, 1]
    }
  }
  output_shapes {
    key: "labels"
    value {
      values: [-1]
    }
  }
  output_shapes {
    key: "input_length"
    value {
      values: [1]
    }
  }
  output_shapes {
    key: "label_length"
    value {
      values: [1]
    }
  }
}
map_callable {
  function_name: "/callable/split_xy"
}
padded_batch {
  shapes_groups {
    named_shapes {
      name: "features"
      shapes {
        values: [-1, 10, 1]
      }
    }
    named_shapes {
      name: "labels"
      shapes {
        values: [-1]
      }
    }
    named_shapes {
      name: "input_length"
      shapes {
        values: [1]
      }
    }
    named_shapes {
      name: "label_length"
      shapes {
        values: [1]
      }
    }
  }
  shapes_groups {
  }
}
batch_size: 10
shuffle_buffer_size: 1024
repeat: 1
"""

_TRAINER_CONFIG_PBTXT = """
  user_defined_python_module: ["toolkit.ml.register_callable"]
  train_dataset {
    %s
  }
  validation_dataset {
    %s
  }
  evaluation_dataset {
    %s
  }
  fit_config {
    epochs: 1
    steps_per_epoch: 10
    validation_steps: 10
  }
  evaluate_config {
    steps: 10
  }
  save_model_config {
    output_directory: ""  
    save_architecture: true
    save_weights: true
    save_full_model: true
  }
  model_config {
    name: "cnn_ctc_model"
    description: "cnn ctc model for asr"
    adadelta_optimizer {
      lr: 1.0
      rho: 0.95
      epsilon: 1e-7
      decay: 0.0
    }
    layer {
      name: "features"
      input {
        shape: [-1, 10, 1]
        dtype: "float32"
        sparse: false
      }
    }
    layer {
      name: "labels"
      input {
        shape: [-1]
        dtype: "int32"
        sparse: false
      }
    }
    layer {
      name: "input_length"
      input {
        shape: [1]
        dtype: "int32"
        sparse: false
      }
    }
    layer {
      name: "label_length"
      input {
        shape: [1]
        dtype: "int32"
        sparse: false
      }
    }
    layer {
      name: "conv1"
      conv_2d {
        filters: 32
        kernel_size: [3, 3]
        strides: [1, 1]
        padding: PADDING_TYPE_SAME
        data_format: DATA_FORMAT_CHANNELS_LAST
        activation: ACTIVATION_TYPE_RELU
      }
      dependency: ["features"]
    }
    layer {
      name: "pool1"
      max_pooling_2d {
        pool_size: [1, 2]
        strides: [1, 2]
        padding: PADDING_TYPE_SAME
        data_format: DATA_FORMAT_CHANNELS_LAST
      }
      dependency: ["conv1"]
    }
    layer {
      name: "cnn_flattened"
      lambda_layer {
        callable_registry {
          closure {
            function_name: "/callable/create_batch_features_fn"
            argument {
              key: "feature_dim"
              value {
                int32_value: 160
              }
            }
          }
        }
      }
      dependency: ["pool1"]
    }
    layer {
      name: "probs"
      dense {
        units: 10
        activation: ACTIVATION_TYPE_SOFTMAX
      }
      dependency: ["cnn_flattened"]
      is_output: true
    }
    layer {
      name: "ctc_batch_cost"
      ctc_batch_cost {
        y_true: "labels"
        y_pred: "probs"
        input_length: "input_length"
        label_length: "label_length"
      }
      dependency: ["labels", "probs", "input_length", "label_length"]
      is_output: true
    }
    loss_spec {
        target_name: "ctc_batch_cost"
        custom_function: "/callable/ctc_loss"
    }
  }
""" % (_DATASET_CONFIG_PBTXT, _DATASET_CONFIG_PBTXT, _DATASET_CONFIG_PBTXT)


class CtcModelTest(unittest.TestCase):

  def setUp(self):
    self._trainer_config = train_pb2.ModelTrainerConfig()
    text_format.Parse(_TRAINER_CONFIG_PBTXT, self._trainer_config)
    output_directory = os.path.join(os.environ["TEST_TMPDIR"], "mnist_model")
    if not os.path.exists(output_directory):
      os.mkdir(output_directory)
    self._trainer_config.save_model_config.output_directory = output_directory
    self._trainer = ModelTrainer()
    self._trainer.init_from_config(trainer_config=self._trainer_config)

  def testTrainAndEvaluate(self):
    self._trainer.train()
    eval_result = self._trainer.evaluate()
    logging.info(eval_result)


if __name__ == '__main__':
  logging.set_verbosity(logging.INFO)
  unittest.main()
