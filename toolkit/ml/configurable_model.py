from absl import logging
from google.protobuf import text_format

import tensorflow as tf

import toolkit.common.callable_pb2 as callable_pb2
import toolkit.common.global_variable as gv
import toolkit.ml.configurable_model_pb2 as configurable_model_pb2
from toolkit.ml.distribution_util import get_distribution_strategy
from tensorflow.keras import backend as K

GVR = gv.GLOBAL_VARIABLE_REPOSITORY


def _non_empty_string_or_none(value):
  return None if not value else value


def _padding_type_to_string(padding_type):
  if padding_type == configurable_model_pb2.PADDING_TYPE_VALID:
    return "valid"
  elif padding_type == configurable_model_pb2.PADDING_TYPE_SAME:
    return "same"
  raise ValueError("invalid padding_type %s" % (padding_type))


def _data_format_to_string(data_format):
  if data_format == configurable_model_pb2.DATA_FORMAT_CHANNELS_FIRST:
    return "channels_first"
  elif data_format == configurable_model_pb2.DATA_FORMAT_CHANNELS_LAST:
    return "channels_last"
  else:
    raise ValueError("invalid data format: %s" % (data_format))


def _shape_from_list(values, return_scalar=False):
  result = tuple([None if v <= 0 else v for v in values])
  return result[0] if len(result) == 1 and return_scalar else result


def _activation_type_to_string(activation_type):
  if activation_type == configurable_model_pb2.ACTIVATION_TYPE_TAHN:
    return "tahn"
  elif activation_type == configurable_model_pb2.ACTIVATION_TYPE_SOFTMAX:
    return "softmax"
  elif activation_type == configurable_model_pb2.ACTIVATION_TYPE_ELU:
    return "elu"
  elif activation_type == configurable_model_pb2.ACTIVATION_TYPE_SELU:
    return "selu"
  elif activation_type == configurable_model_pb2.ACTIVATION_TYPE_SOFTPLUS:
    return "softplus"
  elif activation_type == configurable_model_pb2.ACTIVATION_TYPE_SOFTSIGN:
    return "softsign"
  elif activation_type == configurable_model_pb2.ACTIVATION_TYPE_RELU:
    return "relu"
  elif activation_type == configurable_model_pb2.ACTIVATION_TYPE_SIGMOID:
    return "sigmoid"
  elif activation_type == configurable_model_pb2.ACTIVATION_TYPE_HARD_SIGMOID:
    return "hard_sigmoid"
  elif activation_type == configurable_model_pb2.ACTIVATION_TYPE_EXPONENTIAL:
    return "exponential"
  elif activation_type == configurable_model_pb2.ACTIVATION_TYPE_LINEAR:
    return "linear"
  else:
    raise ValueError("invalid activation_type %s" % (activation_type))


def _create_sgd_optimizer(config):
  return tf.keras.optimizers.SGD(
      lr=config.lr,
      momentum=config.momentum,
      decay=config.decay,
      nesterov=config.nesterov)


def _create_rmsprop_optimizer(config):
  return tf.keras.optimizers.RMSprop(
      lr=config.lr, rho=config.rho, epsilon=config.epsilon, decay=config.decay)


def _create_adagrad_optimizer(config):
  return tf.keras.optimizers.Adagrad(
      lr=config.lr, epsilon=config.epsilon, decay=config.decay)


def _create_adadelta_optimizer(config):
  return tf.keras.optimizers.Adadelta(
      lr=config.lr, rho=config.rho, epsilon=config.epsilon, decay=config.decay)


def _create_adam_optimizer(config):
  return tf.keras.optimizers.Adam(
      learning_rate=config.lr,
      beta_1=config.beta_1,
      beta_2=config.beta_2,
      epsilon=config.epsilon,
      amsgrad=config.amsgrad)


def _retrieve_callable(function_name):
  registry = callable_pb2.CallableRegistry()
  registry.function_name = function_name
  return GVR.retrieve_callable(registry)


class ConfigurableModel(object):

  def __init__(self):
    self._model_config = configurable_model_pb2.ModelConfig()
    self._inputs = []
    self._outputs = []
    self._tensor_lookup = {}
    self._name_to_layer_config = {}
    self._model = None
    # Key is function name and value is a K.function instance.
    self._test_function_lookup = {}

  @property
  def model(self):
    return self._model

  @model.setter
  def model(self, value: tf.keras.models.Model):
    self._model = value

  def get_test_function(self, name):
    return self._test_function_lookup.get(name, None)

  def _create_loss(self):
    loss = {}
    for spec in self._model_config.loss_spec:
      which_loss = spec.WhichOneof("loss")
      if which_loss == "predefined":
        loss[spec.target_name] = configurable_model_pb2.LossType.Name(
            spec.predefined)[len("LOSS_TYPE_"):].lower()
      elif which_loss == "custom_function":
        loss[spec.target_name] = _retrieve_callable(spec.custom_function)
      else:
        raise ValueError("Loss must be specified for target %s." %
                         spec.target_name)
    return loss

  def _create_metric(self):
    metrics = {}
    for spec in self._model_config.metric_spec:
      which_metric = spec.WhichOneof("metric")
      if which_metric == "predefined":
        metrics[spec.target_name] = configurable_model_pb2.MetricType.Name(
            spec.predefined)[len("METRIC_TYPE_"):].lower()
      elif which_metric == "custom_function":
        metrics[spec.target_name] = _retrieve_callable(spec.custom_function)
      else:
        raise ValueError("Metric must be specified for target %s" %
                         spec.target_name)
    return metrics

  def _create_optimizer(self):
    if self._model_config.HasField("sgd_optimizer"):
      return _create_sgd_optimizer(self._model_config.sgd_optimizer)
    elif self._model_config.HasField("rmsprop_optimizer"):
      return _create_rmsprop_optimizer(self._model_config.rmsprop_optimizer)
    elif self._model_config.HasField("adagrad_optimizer"):
      return _create_adagrad_optimizer(self._model_config.adagrad.optimizers)
    elif self._model_config.HasField("adadelta_optimizer"):
      return _create_adadelta_optimizer(self._model_config.adadelta_optimizer)
    elif self._model_config.HasField("adam_optimizer"):
      return _create_adam_optimizer(self._model_config.adam_optimizer)
    else:
      return None

  def _create_layers(self):
    for layer_config in self._model_config.layer:
      self._evaluate_layer(layer_config)

  def init_from_config(self, model_config):
    self._model_config = model_config
    self._name_to_layer_config = dict(
        [l.name, l] for l in self._model_config.layer)
    distribution_strategy = get_distribution_strategy(
        self._model_config.distribution_strategy)
    # Only create the layers under the scope if the distribution_strategy exists
    # and it's not TPUDistributionStrategy.
    if (distribution_strategy and not isinstance(
        distribution_strategy, tf.distribute.experimental.TPUStrategy)):
      with distribution_strategy.scope():
        self._create_layers()
    else:
      self._create_layers()
    # Create test functions
    for function_spec in self._model_config.test_functions:
      inputs = [self._tensor_lookup[layer] for layer in function_spec.inputs]
      outputs = [self._tensor_lookup[layer] for layer in function_spec.outputs]
      logging.info("Create test function: %s" % (function_spec.name))
      self._test_function_lookup[function_spec.name] = K.function(
          inputs, outputs)

    self._model = tf.keras.models.Model(
        name=self._model_config.name,
        inputs=self._inputs,
        outputs=self._outputs)
    if isinstance(distribution_strategy,
                  tf.distribute.experimental.TPUStrategy):
      logging.info("Convert the model into TPU model.")
      self._model = tf.contrib.tpu.keras_to_tpu_model(
          self._model, strategy=distribution_strategy)
    optimizer = self._create_optimizer()
    assert optimizer, "Optimizer is not set."
    losses = self._create_loss()
    metrics = self._create_metric()
    self._model.compile(optimizer=optimizer, loss=losses, metrics=metrics)

  def _evaluate_layer(self, layer_config):
    if layer_config.name in self._tensor_lookup:
      return self._tensor_lookup[layer_config.name]
    if layer_config.HasField("ctc_batch_cost"):
      ctc_config = layer_config.ctc_batch_cost
      ordered_arguments = [
          ctc_config.y_true, ctc_config.y_pred, ctc_config.input_length,
          ctc_config.label_length
      ]
      if not all(ordered_arguments[i] == layer_config.dependency[i]
                 for i in range(len(ordered_arguments))):
        raise ValueError(
            "The dependency for CtcBatchCostLayer must be specified in the "
            "order of the ctc_batch_cost function arguments")
    dependent_tensors = [
        self._evaluate_layer(self._name_to_layer_config[d])
        for d in layer_config.dependency
    ]
    layer = None
    which_layer = layer_config.WhichOneof("specific_layer")
    if which_layer == "input":
      layer = self._create_input_layer(layer_config.name, layer_config.input)
      self._inputs.append(layer)
    elif which_layer == "flatten":
      layer = self._create_flatten_layer(layer_config.name,
                                         layer_config.flatten)
    elif which_layer == "dropout":
      layer = self._create_dropout_layer(layer_config.name,
                                         layer_config.dropout)
    elif which_layer == "conv_2d":
      layer = self._create_conv_2d_layer(layer_config.name,
                                         layer_config.conv_2d)
    elif which_layer == "max_pooling_2d":
      layer = self._create_max_pooling_2d_layer(layer_config.name,
                                                layer_config.max_pooling_2d)
    elif which_layer == "activation":
      layer = self._create_activation_layer(layer_config.name,
                                            layer_config.activation)
    elif which_layer == "zero_padding_2d":
      layer = self._create_zero_padding_2d_layer(layer_config.name,
                                                 layer_config.zero_padding)
    elif which_layer == "l2_normalization":
      layer = self._create_l2_normalization_layer(layer_config.name,
                                                  self.l2_normalization)
    elif which_layer == "dense":
      layer = self._create_dense_layer(layer_config.name, layer_config.dense)
    elif which_layer == "lambda_layer":
      layer = self._create_lambda_layer(layer_config.name,
                                        layer_config.lambda_layer)
    elif which_layer == "lstm_layer":
      layer = self._create_lstm_layer(layer_config.name,
                                      layer_config.lstm_layer)
    elif which_layer == "batch_normalization_layer":
      layer = self._create_batch_normalization_layer(
          layer_config.name, layer_config.batch_normalization_layer)
    if layer is None:
      raise ValueError("invalid LayerConfig with missing layer specification.")
    if dependent_tensors:
      if which_layer in ["ctc_batch_cost", "lambda_layer"]:
        layer_output = layer(dependent_tensors)
      else:
        layer_output = layer(*dependent_tensors)
    else:
      layer_output = layer
    self._tensor_lookup[layer_config.name] = layer_output
    if layer_config.is_output:
      self._outputs.append(layer_output)
    return layer_output

  def _create_lambda_layer(self, name: str,
                           layer: configurable_model_pb2.LambdaLayer):
    return tf.keras.layers.Lambda(
        name=name, function=GVR.retrieve_callable(layer.callable_registry))

  def _create_lstm_layer(self, name: str,
                         layer: configurable_model_pb2.LstmLayer):
    return tf.keras.layers.LSTM(
        name=name,
        units=layer.units,
        return_sequences=layer.return_sequences,
        use_bias=layer.use_bias,
        go_backwards=layer.go_backwards)

  def _create_batch_normalization_layer(
      self, name: str, layer: configurable_model_pb2.BatchNormalizationLayer):
    return tf.keras.layers.BatchNormalization()

  def _create_dense_layer(self, name, layer):
    dense_name = name if not layer.is_time_distributed else name + "_dense"
    dense = tf.keras.layers.Dense(
        name=dense_name,
        units=layer.units,
        activation=_activation_type_to_string(layer.activation),
        use_bias=layer.use_bias)
    return tf.keras.layers.TimeDistributed(
        name=name, layer=dense) if layer.is_time_distributed else dense

  def _create_dropout_layer(self, name: str,
                            layer: configurable_model_pb2.DropoutLayer
                           ) -> tf.keras.layers.Dropout:
    return tf.keras.layers.Dropout(
        name=name,
        rate=layer.rate,
        noise_shape=(layer.noise_shape if layer.noise_shape else None),
        seed=(layer.seed if layer.seed else None))

  def _create_flatten_layer(self, name: str,
                            layer: configurable_model_pb2.FlattenLayer
                           ) -> tf.keras.layers.Flatten:
    del layer
    return tf.keras.layers.Flatten(name=name)

  def _create_input_layer(self, name, layer):
    return tf.keras.layers.Input(
        name=name,
        shape=_shape_from_list(layer.shape),
        batch_size=layer.batch_size if layer.batch_size else None,
        dtype=_non_empty_string_or_none(layer.dtype),
        sparse=layer.sparse)

  def _create_conv_2d_layer(self, name, layer):
    return tf.keras.layers.Conv2D(
        name=name,
        filters=layer.filters,
        kernel_size=_shape_from_list(layer.kernel_size, return_scalar=True),
        strides=tuple(layer.strides),
        padding=_padding_type_to_string(layer.padding),
        data_format=_data_format_to_string(layer.data_format),
        use_bias=layer.use_bias,
        activation=_activation_type_to_string(layer.activation))

  def _create_max_pooling_2d_layer(self, name, layer):
    return tf.keras.layers.MaxPooling2D(
        name=name,
        pool_size=_shape_from_list(layer.pool_size, return_scalar=True),
        strides=_shape_from_list(layer.strides, return_scalar=True),
        padding=_padding_type_to_string(layer.padding),
        data_format=_data_format_to_string(layer.data_format))

  def _create_activation_layer(self, name, layer):
    return tf.keras.layers.Activation(
        name=name, activation=_activation_type_to_string(layer.type))

  def _create_zero_padding_2d_layer(self, name, layer):
    return tf.keras.layers.ZeroPadding2D(
        name=name,
        padding=_shape_from_list(layer.padding, return_scalar=True),
        data_format=_data_format_to_string(layer.data_format))

  def _create_l2_normalization_layer(self, name, layer):
    raise UnimplementedError("not implemented")
