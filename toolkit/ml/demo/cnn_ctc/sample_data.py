import pickle

from absl import logging
from absl import flags
from absl import app

flags.DEFINE_string("input_pickle_file", None, "input pickle file path.")
flags.DEFINE_string("output_pickle_file", None, "output pickle file path.")

FLAGS = flags.FLAGS


def main(argv):
  del argv
  example_features = pickle.load(open(FLAGS.input_pickle_file, "rb"))
  logging.info("Total number of samples: %d" % len(example_features))
  with open(FLAGS.output_pickle_file, "wb") as f:
    logging.info("Write sample to: %s" % FLAGS.output_pickle_file)
    pickle.dump(example_features[0:1000], f)


if __name__ == "__main__":
  app.run(main)
