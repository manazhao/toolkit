# Script demonstrating training and evaluating a classifier for mnist dataset.
MFCC_FEATURE_DIM=13
NUM_CLASSES=28
GENERATOR_CONFIG=$(cat <<EOF
generator_dataset {
  callable_registry {
    closure {
      function_name: "/callable/create_common_voice_generator"
      argument {
        key: "pickle_file"
        value {
          string_value: "<PICKLE_FILE_PATH>"
        }
      }
    }
  }
  output_types {
    key: "feature_input"
    value: "float32"
  }
  output_types {
    key: "label_input"
    value: "int32"
  }
  output_types {
    key: "input_length_input"
    value: "int32"
  }
  output_types {
    key: "label_length_input"
    value: "int32"
  }
  output_shapes {
    key: "feature_input"
    value {
      values: [-1, ${MFCC_FEATURE_DIM}]
    }
  }
  output_shapes {
    key: "label_input"
    value {
      values: [-1]
    }
  }
  output_shapes {
    key: "input_length_input"
    value {
      values: [1]
    }
  }
  output_shapes {
    key: "label_length_input"
    value {
      values: [1]
    }
  }
}
map_callable {
  function_name: "/callable/ctc_transform_input"
}
map_callable {
  function_name: "/callable/split_ctc_batch_cost"
}
padded_batch {
  shapes_groups {
    named_shapes {
      name: "feature_input"
      shapes {
        values: [-1, ${MFCC_FEATURE_DIM}, 1]
      }
    }
    named_shapes {
      name: "label_input"
      shapes {
        values: [-1]
      }
    }
    named_shapes {
      name: "input_length_input"
      shapes {
        values: [1]
      }
    }
    named_shapes {
      name: "label_length_input"
      shapes {
        values: [1]
      }
    }
  }
  shapes_groups {
  }
}
batch_size: 128
repeat: 1000
EOF
)

TRAIN_DATA_CONFIG="$(echo $GENERATOR_CONFIG | \
  sed -e "s|<PICKLE_FILE_PATH>|/data/asr/common_voice_en/train.pkl|g")"
VALIDATION_DATA_CONFIG="$(echo $GENERATOR_CONFIG | \
  sed -e "s|<PICKLE_FILE_PATH>|/data/asr/common_voice_en/dev.pkl|g")"
CONFIG_FILE=$(mktemp -u)
cat >${CONFIG_FILE} <<EOF
user_defined_python_module: [
    "toolkit.ml.register_callable",
    "toolkit.ml.demo.cnn_ctc.module"
]
train_dataset {
$TRAIN_DATA_CONFIG
}
validation_dataset {
$VALIDATION_DATA_CONFIG
}
fit_config {
  epochs: 500
  steps_per_epoch: 100
  validation_steps: 50
}
evaluate_config {
  steps: 500 
}
checkpoint_config {
  filepath: "/tmp/cnn_ctc_model/cp-{epoch:04d}.ckpt"
  save_weights_only: true
  monitor: "val_loss"
  mode: SAVE_MODE_MIN
}
tensor_board_config {
  log_dir: "/tmp/cnn_ctc_model/"
  samples: 20
}
model_config {
  # distribution_strategy {
  #   mirrored_strategy {} 
  # }
  name: "cnn_ctc_model"
  description: "simple cnn for common voice dataset."
  adam_optimizer {
    lr: 0.001,
    beta_1: 0.9,
    beta_2: 0.999,
    epsilon: 1e-07
  }
  layer {
    name: "feature_input"
    input {
      shape: [-1, ${MFCC_FEATURE_DIM}, 1]
      dtype: "float32"
      sparse: false
    }
  }
  layer {
    name: "label_input"
    input {
      shape: [-1]
      dtype: "int32"
      sparse: false
    }
  }
  layer {
    name: "input_length_input"
    input {
      shape: [1]
      dtype: "int32"
      sparse: false
    }
  }
  layer {
    name: "label_length_input"
    input {
      shape: [1]
      dtype: "int32"
      sparse: false
    }
  }
  layer {
    name: "conv1"
    conv_2d {
      filters: 128
      kernel_size: [5, 3]
      strides: [1, 1]
      padding: PADDING_TYPE_SAME
      data_format: DATA_FORMAT_CHANNELS_LAST
      activation: ACTIVATION_TYPE_RELU
    }
    dependency: ["feature_input"]
  }
  layer {
    name: "pool1"
    max_pooling_2d {
      pool_size: [1, 3]
      strides: [1, 2]
      padding: PADDING_TYPE_SAME
      data_format: DATA_FORMAT_CHANNELS_LAST
    }
    dependency: ["conv1"]
  }
  layer {
    name: "conv2"
    conv_2d {
      filters: 128
      kernel_size: [5, 3]
      strides: [1, 1]
      padding: PADDING_TYPE_SAME
      data_format: DATA_FORMAT_CHANNELS_LAST
      activation: ACTIVATION_TYPE_RELU
    }
    dependency: ["pool1"]
  }
  # layer {
  #   name: "conv3"
  #   conv_2d {
  #     filters: 128
  #     kernel_size: [5, 3]
  #     strides: [1, 1]
  #     padding: PADDING_TYPE_SAME
  #     data_format: DATA_FORMAT_CHANNELS_LAST
  #     activation: ACTIVATION_TYPE_RELU
  #   }
  #   dependency: ["conv2"]
  # }
  # layer {
  #   name: "conv4"
  #   conv_2d {
  #     filters: 128
  #     kernel_size: [5, 3]
  #     strides: [1, 1]
  #     padding: PADDING_TYPE_SAME
  #     data_format: DATA_FORMAT_CHANNELS_LAST
  #     activation: ACTIVATION_TYPE_RELU
  #   }
  #   dependency: ["conv3"]
  # }
  layer {
    name: "cnn_flattened"
    lambda_layer {
      callable_registry {
        closure {
          function_name: "/callable/create_batch_features_fn"
          argument {
            key: "feature_dim"
            value {
              int32_value: 896
            }
          }
        }
      }
    }
    dependency: ["conv2"]
  }
  layer {
    name: "fc1"
    dense {
      units:  256
      activation: ACTIVATION_TYPE_RELU
    }
    dependency: ["cnn_flattened"]
    is_output: false
  }
  # layer {
  #   name: "fc2"
  #   dense {
  #     units:  1024
  #     activation: ACTIVATION_TYPE_RELU
  #   }
  #   dependency: ["fc1"]
  #   is_output: false
  # }
  layer {
    name: "pred"
    dense {
      units:  ${NUM_CLASSES}
      activation: ACTIVATION_TYPE_SOFTMAX
    }
    dependency: ["fc1"]
    is_output: false
  }
  layer {
    name: "decode"
    lambda_layer {
      callable_registry {
        closure {
          function_name: "/callable/create_ctc_decode_fn"
        }
      }
    }
    dependency: ["pred", "input_length_input"]
    is_output: false
  }
  layer {
    name: "ctc_batch_cost"
    lambda_layer {
      callable_registry {
        function_name: "/callable/ctc_batch_cost"
      }
    }
    dependency: [
      "label_input",
      "pred",
      "input_length_input",
      "label_length_input"
    ]
    is_output: true
  }
  loss_spec {
      target_name: "ctc_batch_cost"
      custom_function: "/callable/ctc_loss"
  }
  test_functions {
    name: "decode_audio_features"
    inputs: ["feature_input", "input_length_input"]
    outputs: ["decode"]
  }
}
create_callbacks {
  closure {
    function_name: "/callable/create_model_validation_callabck_fn",
    argument {
      key: "pickle_file"
      value {
        string_value: "/data/asr/common_voice_en/test.pkl"
      }
    }
    argument {
      key: "pred_function"
      value {
        string_value: "decode_audio_features"
      }
    }
  }
}
EOF

bazel run toolkit/ml:train -- \
  --trainer_config_file=${CONFIG_FILE} \
  --job=train
