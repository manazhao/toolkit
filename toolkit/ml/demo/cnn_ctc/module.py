import pickle
import string

from absl import logging
from typing import Any, Callable, Tuple, Mapping, TypeVar

import numpy as np
import tensorflow as tf
import toolkit.common.global_variable as gv

from toolkit.ml import model_trainer as mt

GVR = gv.GLOBAL_VARIABLE_REPOSITORY
_DUMMY_TARGET = 0.0


def _create_generator(pickle_file):
  example_features = pickle.load(open(pickle_file, "rb"))
  n_samples = len(example_features)

  def _generate_features():
    i = 0
    while True:
      i += 1
      example = example_features[i % n_samples]
      if example["label_input"].shape[0] == 0:
        continue
      if not isinstance(example["input_length_input"], list):
        example["input_length_input"] = [example["input_length_input"]]
      if not isinstance(example["label_length_input"], list):
        example["label_length_input"] = [example["label_length_input"]]
      yield example

  return _generate_features


def _ctc_transform_input(feature_map):
  feature_map["feature_input"] = tf.expand_dims(feature_map["feature_input"], 2)
  return feature_map


def _split_ctc_batch_cost(feature_map):
  return feature_map, _DUMMY_TARGET


class _ModelValidationCallback(tf.keras.callbacks.Callback):

  def __init__(self, examples, pred_function):
    self._index = 0
    self._trainer = None
    self._examples = examples
    self._pred_function = pred_function

  def set_trainer(self, trainer: mt.ModelTrainer):
    self._trainer = trainer

  def _tokens_to_text(self, tokens):
    LETTER_LOOKUP = " " + "".join(string.ascii_lowercase) + "|"
    return "".join([LETTER_LOOKUP[i] for i in tokens])

  def on_epoch_end(self, epoch, logs=None):
    test_func = self._trainer.configurable_model.get_test_function(
        self._pred_function)

    num_examples = len(self._examples)
    for i in range(5):
      example = self._examples[self._index % num_examples]
      feature_input = np.expand_dims(
          np.expand_dims(example["feature_input"], axis=2), axis=0)
      input_length = np.expand_dims([example["input_length_input"]], axis=0)
      out = test_func([feature_input, input_length])
      decoded_text = self._tokens_to_text(out[0][0][0])
      logging.info("original text: %s" %
                   (self._tokens_to_text(example["label_input"])))
      logging.info("decoded text: %s" % (decoded_text))
      self._index = self._index + 1


def _create_model_validation_callabck_fn(pickle_file: str, pred_function: str):
  examples = pickle.load(open(pickle_file, "rb"))

  def _create_callback():
    return _ModelValidationCallback(examples, pred_function)

  return _create_callback


GVR.register_callable("/callable/create_common_voice_generator",
                      _create_generator)
GVR.register_callable("/callable/split_ctc_batch_cost", _split_ctc_batch_cost)
GVR.register_callable("/callable/ctc_transform_input", _ctc_transform_input)
GVR.register_callable("/callable/create_model_validation_callabck_fn",
                      _create_model_validation_callabck_fn)
