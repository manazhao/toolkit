import numpy as np

import pyroomacoustics as pra


class RoomSimulator(object):

  def __init__(self):
    self._room_dim_range = [5, 20]
    self._absorption_range = [0.1, 0.6]
    self._max_order_range = [1, 20]

  def _random_range(self, num_samples, start, end):
    return np.random.random(num_samples if num_samples > 1 else None) * (
        end - start) + start

  def generate(self, target_signal, noise_signal, fs, output_wave_file):
    # Create the shoebox
    room_dim = self._random_range(3, self._room_dim_range[0],
                                  self._room_dim_range[1]).astype(int)
    print(target_signal)
    shoebox = pra.ShoeBox(
        room_dim,
        absorption=self._random_range(1, self._absorption_range[0],
                                      self._absorption_range[1]),
        fs=fs,
        max_order=int(
            self._random_range(1, self._max_order_range[0],
                               self._max_order_range[1])),
    )
    target_location = (room_dim * np.random.random(3)).astype(int)
    noise_location = (room_dim * np.random.random(3)).astype(int)
    mic_location = (room_dim * np.random.random(3)).astype(int)
    shoebox.add_source(target_location, signal=target_signal)
    shoebox.add_source(noise_location, signal=noise_signal)
    shoebox.add_microphone_array(
        pra.MicrophoneArray(np.array([mic_location]).T, shoebox.fs))
    shoebox.simulate()
    audio_reverb = shoebox.mic_array.to_wav(
        output_wave_file, norm=True, bitdepth=np.int16)
