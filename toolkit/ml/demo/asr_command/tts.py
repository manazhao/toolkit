import os
from collections import namedtuple

from absl import app
from absl import flags
from absl import logging
from google.cloud import texttospeech
from scipy.io import wavfile

import numpy as np

flags.DEFINE_string("command_file", None, "File containing a list of commands.")
flags.DEFINE_string("output_directory", None,
                    "Directory holding the audio clips for the commands.")

FLAGS = flags.FLAGS


def _tts(client, text, audio_file):
  synthesis_input = texttospeech.types.SynthesisInput(text=text)
  voice = texttospeech.types.VoiceSelectionParams(
      language_code='en-US',
      ssml_gender=texttospeech.enums.SsmlVoiceGender.NEUTRAL)
  audio_config = texttospeech.types.AudioConfig(
      audio_encoding=texttospeech.enums.AudioEncoding.LINEAR16)
  # voice parameters and audio file type
  response = client.synthesize_speech(synthesis_input, voice, audio_config)
  with open(audio_file, 'wb') as out:
    logging.info("Write audio file: %s" % audio_file)
    out.write(response.audio_content)


def main(argv):
  with open(FLAGS.command_file, "r") as f:
    # Instantiates a client
    client = texttospeech.TextToSpeechClient()
    commands = [line.rstrip('\n') for line in f]
    logging.info("Number of commands: %d" % len(commands))
    for idx, command in enumerate(commands):
      audio_file = os.path.join(FLAGS.output_directory, "%d.wav" % idx)
      _tts(client, command, audio_file)


if __name__ == "__main__":
  app.run(main)
