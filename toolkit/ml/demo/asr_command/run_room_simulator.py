import math
import os

from absl import app
from absl import flags
from absl import logging
from scipy.io import wavfile
import numpy as np
import librosa

from toolkit.ml.demo.asr_command import room_simulator as rs

flags.DEFINE_string(
    "target_audio_file_csv", None,
    "File containing a list of audio files which noisy versions will be "
    "created for.")
flags.DEFINE_string(
    "noise_audio_file_csv", None,
    "File containing a list of audio files used as noise to interfere the "
    "target audio.")
flags.DEFINE_integer(
    "num_variants", 3,
    "Number of noisy versions will be created for each target audio.")
flags.DEFINE_integer("sample_rate", 16000, "audio sampling rate.")
flags.DEFINE_string("output_directory", None,
                    "Directory holding the simulated audio files.")

FLAGS = flags.FLAGS


def _load_noise_audio_signal():
  with open(FLAGS.noise_audio_file_csv, "r") as f:
    lines = [line.rstrip('\n') for line in f]
    signals = []
    for line in lines:
      signal, _ = librosa.load(line, sr=FLAGS.sample_rate)
      signals.append(signal)
    return signals


def _cut_or_padding_signal(signal, target_length):
  if signal.size < target_length:
    repeated_signal = np.tile(
        signal, reps=math.ceil(target_length / signal.size))
    return repeated_signal[0:target_length]
  else:
    start = np.random.randint(0, signal.size - target_length + 1)
    return signal[start:(start + target_length)]


def main(argv):
  with open(FLAGS.target_audio_file_csv, "r") as f:
    noise_signals = _load_noise_audio_signal()
    simulator = rs.RoomSimulator()
    # Instantiates a client
    lines = [line.rstrip('\n') for line in f]
    for line in lines:
      target_signal, _ = librosa.load(line, sr=FLAGS.sample_rate)
      noise_signal = _cut_or_padding_signal(
          np.random.choice(noise_signals, 1)[0], target_signal.size)
      filename, ext = os.path.splitext(os.path.basename(line))
      for i in range(FLAGS.num_variants):
        output_audio_file = os.path.join(FLAGS.output_directory,
                                         "%s_%d%s" % (filename, i, ext))
        logging.info("Write noise variant: %s" % output_audio_file)
        simulator.generate(target_signal, noise_signal, FLAGS.sample_rate,
                           output_audio_file)


if __name__ == "__main__":
  app.run(main)
