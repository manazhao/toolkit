import unittest

from absl import logging
from parameterized import parameterized
import tensorflow as tf

from toolkit.ml.distribution_strategy_pb2 import DistributionStrategyConfig
from toolkit.ml.distribution_strategy_pb2 import MultiWorkerMirroredStrategyConfig
from toolkit.ml.distribution_util import get_distribution_strategy


class DistributionUtilTest(unittest.TestCase):

  def test_none_distribution(self):
    config = DistributionStrategyConfig()
    self.assertIsNone(get_distribution_strategy(config))

  @parameterized.expand([("positive_gpu", 2, 2, 2),
                         ("unspecified_gpu", 0, 1, 1)])
  def test_mirrored_strategy(self, name, num_gpus, expected_replicas,
                             expected_num_worker_devices):
    config = DistributionStrategyConfig()
    config.mirrored_strategy.num_gpus = num_gpus
    strategy = get_distribution_strategy(config)
    self.assertEqual(strategy.num_replicas_in_sync, expected_replicas)
    self.assertEqual(len(strategy.extended.worker_devices),
                     expected_num_worker_devices)
    logging.info(strategy.extended.worker_devices)

  # def test_multi_worker_mirrored_strategy(self):
  #   config = DistributionStrategyConfig()
  #   config.multi_worker_mirrored_strategy.CopyFrom(
  #       MultiWorkerMirroredStrategyConfig())
  #   strategy = get_distribution_strategy(config)

  # def test_tpu_strategy(self):
  #   config = DistributionStrategyConfig()
  #   config.tpu_strategy.tpu = "fake_tpu_address"
  #   strategy = get_distribution_strategy(config)


if __name__ == '__main__':
  logging.set_verbosity(logging.INFO)
  unittest.main()
