import datetime
import importlib
import os

from absl import logging
from typing import Callable, Any, TypeVar, List, Mapping
from google.protobuf import text_format

import tensorflow as tf

from toolkit.ml.train_pb2 import ModelTrainerConfig
from toolkit.ml.input_pb2 import DatasetConfig
from toolkit.ml.train_pb2 import ModelCheckpointConfig
from toolkit.ml.configurable_model import ConfigurableModel
import toolkit.common.global_variable as gv
import toolkit.ml.input as data_input

GVR = gv.GLOBAL_VARIABLE_REPOSITORY


class ModelTrainer(object):

  def __init__(self):
    self._trainer_config = ModelTrainerConfig()
    self._configurable_model = ConfigurableModel()
    self._train_dataset = None
    self._validation_dataset = None
    self._evaluation_dataset = None

  def _init_model(self):
    # Order of initializing the model:
    # 1. Init the architecture from ConfigurableModel config if
    #    load_model_config is not given.
    # 2. If full_model_path is given, load both the architecture and weights
    #    from the file.
    # 3. Try to initialize the architecture from json_file, yaml_file and then
    #    the ModelConfig.
    # 4. Load the weights if weights_file is given.
    if not self._trainer_config.HasField("load_model_config"):
      self._configurable_model.init_from_config(
          self._trainer_config.model_config)
      return

    load_config = self._trainer_config.load_model_config
    if load_config.model_path:
      logging.info("Load full model path: %s" % (load_config.model_path))
      self._configurable_model.model = tf.keras.models.load_model(
          load_config.model_path)
      return

    if load_config.saved_model_path:
      self._configurable_model.model = \
          tf.keras.experimental.load_from_saved_model(\
          load_config.saved_model_path)
      return

    if load_config.architecture_path:
      with open(load_config.architecture_path) as f:
        json_str = f.read()
        self._configurable_model.model = tf.keras.models.model_from_json(
            json_str)
    if load_config.weights_path:
      self._configurable_model.model.load_weights(load_config.weights_path)

  def init_from_config(self, trainer_config: ModelTrainerConfig) -> bool:
    logging.info("train_config: %s" % str(trainer_config))
    self._trainer_config = trainer_config
    # Load the python modules if they're configured.
    for module_name in self._trainer_config.user_defined_python_module:
      logging.info("Load python module [%s]" % (module_name))
      importlib.import_module(module_name)
    logging.info("create model from config.")
    self._init_model()
    self._configurable_model.model.summary()
    return True

  @property
  def configurable_model(self) -> ConfigurableModel:
    return self._configurable_model

  def _tensorboard_callback(self):
    tensor_board_config = self._trainer_config.tensor_board_config
    update_freq = tensor_board_config.WhichOneof("update_freq")
    if update_freq == "samples":
      update_freq = tensor_board_config.samples
    return tf.keras.callbacks.TensorBoard(
        log_dir=tensor_board_config.log_dir,
        batch_size=tensor_board_config.batch_size,
        write_graph=tensor_board_config.write_graph,
        write_images=tensor_board_config.write_images,
        write_grads=tensor_board_config.write_grads,
        update_freq=update_freq)

  def _checkpoint_callback(self):
    checkpoint_config = self._trainer_config.checkpoint_config
    return tf.keras.callbacks.ModelCheckpoint(
        filepath=checkpoint_config.filepath,
        monitor=(checkpoint_config.monitor
                 if checkpoint_config.monitor else "val_loss"),
        save_best_only=checkpoint_config.save_best_only,
        save_weights_only=checkpoint_config.save_weights_only,
        mode=("auto" if checkpoint_config.mode == ModelCheckpointConfig.
              SAVE_MODE_UNSPECIFIED else checkpoint_config.mode),
        period=(checkpoint_config.period if checkpoint_config.period else 1))

  @property
  def train_dataset(self):
    if self._train_dataset is None:
      logging.info("Create training dataset.")
      self._train_dataset = data_input.get_dataset(
          self._trainer_config.train_dataset)
    return self._train_dataset

  @property
  def validation_dataset(self):
    if self._validation_dataset is None and self._trainer_config.HasField(
        "validation_dataset"):
      logging.info("Create validationing dataset.")
      self._validation_dataset = data_input.get_dataset(
          self._trainer_config.validation_dataset)
    return self._validation_dataset

  def train(self) -> tf.keras.callbacks.History:
    logging.info("start training...")
    fit_config = self._trainer_config.fit_config
    callbacks = []
    if self._trainer_config.HasField("tensor_board_config"):
      callbacks.append(self._tensorboard_callback())
    if self._trainer_config.HasField("checkpoint_config"):
      callbacks.append(self._checkpoint_callback())
    # Add other callbacks.
    for registry in self._trainer_config.create_callbacks:
      tmp_callback = GVR.retrieve_callable(registry)()
      tmp_callback.set_trainer(trainer=self)
      callbacks.append(tmp_callback)
    return self._configurable_model.model.fit(
        x=self.train_dataset,
        y=None,
        validation_data=self.validation_dataset,
        callbacks=callbacks,
        epochs=fit_config.epochs,
        steps_per_epoch=(fit_config.steps_per_epoch
                         if fit_config.steps_per_epoch else None),
        validation_steps=(fit_config.validation_steps
                          if fit_config.validation_steps else None))

  def _mkdir_if_not_exists(self, path):
    if os.path.exists(path):
      logging.info("Path already exists: %s" % (path))
      return
    try:
      original_umask = os.umask(0)
      os.makedirs(path, mode=0o755)
    finally:
      os.umask(original_umask)

  def save_model(self) -> Mapping[str, str]:
    if not self._trainer_config.HasField("save_model_config"):
      logging.warn("SaveModelConfig is not set. Model wont'be saved")
      return

    save_config = self._trainer_config.save_model_config
    epoch = datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
    result_paths = {}
    keras_model = self._configurable_model.model
    if save_config.save_architecture:
      architecture_file = os.path.join(save_config.output_directory,
                                       "architecture_%s.json" % (epoch))
      with open(architecture_file, "w") as f:
        logging.info("save architecture as json into: %s" % (architecture_file))
        f.write(keras_model.to_json())
        result_paths["architecture"] = architecture_file
    if save_config.save_weights:
      weights_file = os.path.join(save_config.output_directory,
                                  "weights_%s.h5" % (epoch))
      logging.info("save weights: %s" % (architecture_file))
      self._configurable_model.model.save_weights(weights_file)
      result_paths["weights"] = weights_file
    if save_config.save_tf_saved_model:
      # output directory: <output_directory>/saved_model/<epoch>
      saved_model_path = os.path.join(save_config.output_directory,
                                      "saved_model", epoch)
      self._mkdir_if_not_exists(saved_model_path)
      tf.keras.experimental.export_saved_model(self._configurable_model.model,
                                               saved_model_path)
      result_paths["saved_model"] = saved_model_path
    if save_config.save_full_model:
      full_model_file = os.path.join(save_config.output_directory,
                                     "full_model_%s.h5" % (epoch))
      logging.info("save full model: %s" % (full_model_file))
      self._configurable_model.model.save(full_model_file)
      result_paths["full_model"] = full_model_file
    return result_paths

  def evaluate(self) -> TypeVar("EvalResult", float, List[float]):
    logging.info("Create evaluation dataset.")
    assert self._trainer_config.HasField("evaluation_dataset"), \
        "evaluation dataset must be specified."
    self._evaluation_dataset = data_input.get_dataset(
        self._trainer_config.evaluation_dataset)
    logging.info("start evaluation...")
    return self._configurable_model.model.evaluate(
        self._evaluation_dataset,
        steps=self._trainer_config.evaluate_config.steps)
