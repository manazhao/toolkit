from absl import logging

import tensorflow as tf
from toolkit.ml.distribution_strategy_pb2 import DistributionStrategyConfig
from toolkit.ml.distribution_strategy_pb2 import MirroredStrategyConfig
from toolkit.ml.distribution_strategy_pb2 import MultiWorkerMirroredStrategyConfig
from toolkit.ml.distribution_strategy_pb2 import ParameterServerStrategyConfig
from toolkit.ml.distribution_strategy_pb2 import TpuStrategyConfig


def _mirrored_cross_device_ops(config: MirroredStrategyConfig):
  if (config.cross_device_ops ==
      MirroredStrategyConfig.CROSS_DEVICE_OPS_UNSPECIFIED):
    return None
  if (config.cross_device_ops == MirroredStrategyConfig.CROSS_DEVICE_OPS_NCCL):
    return tf.distribute.NcclAllReduce(num_packs=config.num_packs)
  if (config.cross_device_ops ==
      MirroredStrategyConfig.CROSS_DEVICE_OPS_HIERARCHICAL_COPY):
    return tf.distribute.HierarchicalCopyAllReduce(num_packs=config.num_packs)


def _communication(config: MultiWorkerMirroredStrategyConfig):
  if (config.collective_communication ==
      MultiWorkerMirroredStrategyConfig.COLLECTIVE_COMMUNICATION_UNSPECIFIED or
      config.collective_communication ==
      MultiWorkerMirroredStrategyConfig.COLLECTIVE_COMMUNICATION_AUTO):
    return tf.distribute.experimental.CollectiveCommunication.AUTO

  if config.collective_communication == \
      MultiWorkerMirroredStrategyConfig.COLLECTIVE_COMMUNICATION_RING:
    return tf.distribute.experimental.CollectiveCommunication.RING
  if config.collective_communication == \
      MultiWorkerMirroredStrategyConfig.COLLECTIVE_COMMUNICATION_NCCL:
    return tf.distribute.experimental.CollectiveCommunication.NCCL


def get_distribution_strategy(config: DistributionStrategyConfig
                             ) -> tf.distribute.Strategy:
  which_strategy = config.WhichOneof("specific_strategy")
  if not which_strategy:
    return None
  if which_strategy == "mirrored_strategy":
    tmp_config = config.mirrored_strategy
    logging.info("Num GPUs Available: ",
                 len(tf.config.experimental.list_physical_devices('GPU')))
    return tf.distribute.MirroredStrategy(
        devices=(None if not tmp_config.num_gpus else
                 ["/device:GPU:%d" % (i) for i in range(tmp_config.num_gpus)]),
        cross_device_ops=_mirrored_cross_device_ops(tmp_config))
  if which_strategy == "multi_worker_mirrored_strategy":
    raise NotImplementedError(
        "MultiWorkerMirroredStrategy is not available yet.")
  if which_strategy == "tpu_strategy":
    raise NotImplementedError("TPUStrategy is not available yet.")
    # resolver = tf.contrib.cluster_resolver.TPUClusterResolver(
    #     tpu=config.tpu_strategy.tpu)
    # return tf.distribute.experimental.TPUStrategy(resolver)
