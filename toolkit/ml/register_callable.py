import sys

from absl import logging
from typing import Any, Callable, Tuple, Mapping, TypeVar

import tensorflow as tf
import toolkit.common.global_variable as gv
from tensorflow.keras import backend as K

GVR = gv.GLOBAL_VARIABLE_REPOSITORY

TensorType = TypeVar("TensorType", tf.Tensor, tf.SparseTensor)


def return_prepare_image_input_fn(feature_name: str,
                                  label_name: str,
                                  width_name=None,
                                  height_name=None,
                                  channel_name=None) -> Callable[..., Any]:

  def _input_fn(feature_map: Mapping[str, TensorType]
               ) -> Tuple[TensorType, TensorType]:
    feature_tensor = feature_map.get(feature_name)
    label_tensor = feature_map.get(label_name)
    if feature_tensor is None or label_tensor is None:
      logging.fatal(
          "feature_tensor and label_tensor must exist in the feature map.")
    width_tensor = tf.reshape(feature_map.get(width_name), (1,))
    height_tensor = tf.reshape(feature_map.get(height_name), (1,))
    channel_tensor = tf.reshape(feature_map.get(channel_name), (1,))
    if (width_tensor is not None and height_tensor is not None and
        channel_tensor is not None):
      feature_tensor = tf.reshape(
          feature_tensor,
          shape=tf.concat([width_tensor, height_tensor, channel_tensor],
                          axis=0))
    return feature_tensor, label_tensor

  return _input_fn


# A special loss function for ctc based prediction. `y_pred` is already a loss.
def _ctc_loss(y_true, cost):
  del y_true
  return cost


def _ctc_batch_cost(tensors):
  y_true, y_pred, input_length, label_length = tensors
  cost = tf.keras.backend.ctc_batch_cost(
      y_true=y_true,
      y_pred=y_pred,
      input_length=input_length,
      label_length=label_length)
  return cost


def _create_batch_time_features_fn(feature_dim):

  def _batch_time_features(x):
    x = tf.squeeze(x, axis=0)
    batch_size = tf.shape(x)[0]
    return tf.reshape(x, shape=[batch_size, -1, feature_dim])

  return _batch_time_features


def _create_ctc_decode_fn(greedy=False, beam_width=10, top_paths=1):

  def _decode(tensors):
    y_pred, input_length = tensors
    return K.ctc_decode(
        y_pred=y_pred,
        input_length=tf.reshape(input_length, shape=[-1]),
        greedy=greedy,
        beam_width=beam_width,
        top_paths=top_paths)[0]

  return _decode


GVR.register_callable("/callable/return_prepare_image_input_fn",
                      return_prepare_image_input_fn)
GVR.register_callable("/callable/ctc_loss", _ctc_loss)
GVR.register_callable("/callable/ctc_batch_cost", _ctc_batch_cost)
GVR.register_callable("/callable/create_batch_features_fn",
                      _create_batch_time_features_fn)
GVR.register_callable("/callable/create_ctc_decode_fn", _create_ctc_decode_fn)
